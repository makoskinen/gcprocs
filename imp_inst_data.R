##############################################
# Data import routines for various analysers #
##############################################
#
# Comments:
# I wrote most of these in late 2018 for the Kohl et al 2019 (Biogeosciences) technical note. I currently update only a few of these (mainly Picarro). 
# The routines as initially written have a major flaw: if for some reason the function stops with an error, you end up with the working directory set 
# in some subdirectory of the data directory, and you have to manually set it back to the original R working directory using setwd().
#
# Most of these routines can be simplified by using paste() to combine directory and file names in the read* functions instead of switching directories.
# Also list.files(recursive=T) avoids moving through the different directories to detect files.
#
# Make sure there are not 'foreign' files mixed in with the data files, that's the most common source of error.

#load required packages
require('pbmcapply') #progress bar for 'for' loops


#############import PTRMS file
##Note: the PTRMS records the LOCAL TIME set on the analyzer computer. if not provided, R assumes that time zone is local time zone in your computer. Use 'tz' argument to set the time zone for the imported data
read.ptr <- function(dir, tz="") {
  wd<-getwd()
  setwd(dir)
  
  #read list of files in directory, filter for files that contain .ptc
  file_list<-list.files()
  file_list<-file_list[grep(".ptc", file_list)]
  
  dataset<-NULL
  
  #read all files into a signle data.frame
  for (file in file_list){
    # if the merged dataset doesn't exist, create it
    if (!exists("dataset")){
      dataset <- read.csv(file, header=TRUE, skip = 9, sep="\t")
    }
    # if the merged dataset does exist, append to it
    if (exists("dataset")){
      temp_dataset <-read.csv(file, header=TRUE, skip = 9, sep="\t")
      dataset<-rbind(dataset, temp_dataset)
      rm(temp_dataset)
    }
  }
  setwd(wd)
  
  ## round the time in column 1 to whole seconds, removes the "," used as a decimal point
  sec<-round(as.numeric(paste(substr(dataset$X.Abs.time., 18,19), ".", substr(dataset$X.Abs.time., 21,23), sep="")), digits = 0)
  sec<-formatC(sec, width = 2, format = "d", flag = "0")
  
  #put time/date into yyyy-mm-dd hh:mm:ss format
  dataset$X.Abs.time.<-paste(
    substr(dataset$X.Abs.time., 7,10), "-",
    substr(dataset$X.Abs.time., 4,5), "-",
    substr(dataset$X.Abs.time., 1,2), " ",
    substr(dataset$X.Abs.time., 12,17), sec, sep="")
  
  dataset$X.Abs.time.<-as.POSIXct(dataset$X.Abs.time., tz=tz)
  
  
  #dataset<-dataset[order(as.numeric(dataset$X.Abs.time.)),]
  
  return(dataset)
}

#############import PTRMS file - Kaisa's processed files
##Note: the PTRMS records the LOCAL TIME set on the analyzer computer. if not provided, R assumes that time zone is local time zone in your computer. Use 'tz' argument to set the time zone for the imported data

dir<-"~/lk5255@mun.ca/work/Projects/MeMeTre/VOCTest/VOCtest_20181022ff/PTRMS_Kaisa"

read.ptr.kaisa <- function(dir, tz="") {
  wd<-getwd()
  setwd(dir)
  
  #read list of files in directory, filter for files that contain .ptc
  file_list<-list.files()
  file_list<-file_list[grep(".dat", file_list)]
  
  dataset<-NULL
#  file<-file_list[1]
  #read all files into a signle data.frame
  for (file in file_list){
    # if the merged dataset doesn't exist, create it
    if (!exists("dataset")){
      dataset <- read.csv(file)
    }
    # if the merged dataset does exist, append to it
    if (exists("dataset")){
      temp_dataset <-read.csv(file)
      dataset<-rbind(dataset, temp_dataset)
      rm(temp_dataset)
    }
  }
  setwd(wd)
  
  
  #put time/date into yyyy-mm-dd hh:mm:ss format
  dataset$datetime<-paste(dataset[,2],  "-", dataset[,3], "-", dataset[,4], " ", dataset[,5], ":",dataset[,6], ":",dataset[,7], sep="")
  dataset$datetime<-as.POSIXct(dataset$datetime, tz=tz)
  
  #dataset<-dataset[order(as.numeric(dataset$X.Abs.time.)),]
    return(dataset)
}


############Import Picarro G2301 data
##Note: the PICARROS records the UTC TIME (i.e., GMT). The read.picarro function automatically sets the time zone to UTC.
read.picarro <- function(dir, cols=c("all")) {

  file_list<-list.files(dir, recursive = T)

  dataset<-NULL
  
  imax<-length(file_list)
  pb <- progressBar(min=0, max=imax, style='ETA')
  #i<-1
  #i<-2
  for (i in 1:imax){
    # if the merged dataset doesn't exist, create it
    # print(paste0(dir,file_list[i]))
    if (i==1){
      dataset <- read.table(paste0(dir,file_list[i]), header=TRUE)
      if(cols[1]!='all'){
        dataset <- dataset[,cols]
      }
    } else {# if the merged dataset does exist, append to it
      temp_dataset <-read.table(paste0(dir,file_list[i]), header=TRUE)
      if(cols[1]!='all'){
        temp_dataset <- temp_dataset[,c('DATE','TIME',cols)]
      }
      dataset<-rbind(dataset, temp_dataset)
      rm(temp_dataset)
    }
    setTxtProgressBar(pb, i)
  }
  dataset$datetime<-as.POSIXct(paste(dataset$DATE, substr(dataset$TIME,1,8)), tz = "UTC")
  dataset <- dataset[order(dataset$datetime),]
  return(dataset)  
}


####Import data from old GASMET analyzer


read.gasmet<-function(dir, tz="") {
  wd<-getwd()
  setwd(dir)
  
  dir_list<-list.files()
  dataset<-NULL
  
  
  for (d in dir_list){
    setwd(d)
    
    file_list<-list.files()
    file_list <- file_list[grep (".TXT", file_list)]
    
    for (file in file_list){
      # if the merged dataset doesn't exist, create it
      if (!exists("dataset")){
        dataset <- read.csv(file, header=TRUE, sep="\t", fileEncoding="latin1")
      }
      # if the merged dataset does exist, append to it
      if (exists("dataset")){
        temp_dataset <-read.table(file, header=TRUE, sep="\t", fileEncoding="latin1")
        dataset<-rbind(dataset, temp_dataset)
        rm(temp_dataset)
      }
    }
    
    setwd("..")
  }
  setwd(wd)
  
  dataset$datetime<-as.POSIXct(paste(dataset$Date, dataset$Time), tz=tz)
  return(dataset)
}


####Import data from new GASMET analyzer
read.gasmet2<-function(dir, tz="") {
  wd<-getwd()
  setwd(dir)
  
  dir_list<-list.files()
  dataset<-NULL
  
#  d<-dir_list[1]
  for (d in dir_list){
    setwd(d)
    file_list<-list.files()
    file_list <- file_list[grep (".TXT", file_list)]
    #file<-file_list[1]
    for (file in file_list){
      # if the merged dataset doesn't exist, create it
      if (!exists("dataset")){
        dataset <- read.csv(file, header=TRUE, sep="\t", fileEncoding="latin1")
      }
      # if the merged dataset does exist, append to it
      if (exists("dataset")){
        temp_dataset <-read.table(file, header=TRUE, sep="\t", fileEncoding="latin1")
        dataset<-rbind(dataset, temp_dataset)
        rm(temp_dataset)
      }
    }
    
    setwd("..")
  }
  setwd(wd)
  
  dataset<-dataset[dataset$Date!='Date',]
  dataset$datetime<-as.POSIXct(paste(dataset$Date, dataset$Time), tz=tz)
  return(dataset)
}

#################################
####Import data from LGR UGGA ###
#################################

merge.UGGA.files <- function(path, tz=""){ 
  
  #Create an empty data frame
  d <- c()
  
  #Create a list of the wated files within date folder
  files <- list.files(path = path,pattern = paste("gga_.*_f",sep = ""))
  
  #Combine the txt/zip files 
  for (i in 1:length(files)) {
    if (substring(files[i],first = nchar(files[i])-3) == ".zip") {
      b <- read.table(unz(paste(path,"/",files[i],sep = ""),
                          substring(files[i],first = 1,last = nchar(files[i])-4)),header = T,skip = 1,
                      sep = ",",fill = T,colClasses = c("character", rep("numeric", 22), "character"))
      b <- subset(b,b[,22] != "NA", select = 1:24)
      d <- rbind(d,b)
    } else {
      b <- read.table(paste(path,"/",files[i],sep = ""),header = T,skip = 1,sep = ",",fill = T,
                      colClasses = c("character", rep("numeric", 22), "character"))
      b <- subset(b,b[,22] != "NA", select = 1:24)
      d <- rbind(d,b)
    }
  }
  
  #Rename the columns
  names(d) <- c("Time","CH4_ppm","CH4_ppm_sd","H2O_ppm","H2O_ppm_sd","CO2_ppm","CO2_ppm_sd",
                "CH4_d_ppm","CH4_d_ppm_sd","CO2_d_ppm","CO2_d_ppm_sd","GasP_torr","GasP_torr_sd",
                "GasT_C","GasT_C_sd","AmbT_C","AmbT_C_sd","RD0_us","RD0_us_sd","RD1_us",
                "RD1_us_sd","Fit_Flag","MIU_VALVE","MIU_DESC")
  
  #set dates and times to POSIXct
  d$Time <- ISOdatetime(substring(d$Time, 9,12),substring(d$Time, 6,7),substring(d$Time, 3,4),
                        substring(d$Time, 14,15),substring(d$Time, 17,18),substring(d$Time, 20,21), tz=tz)
  return(d)
}

read.ugga<-function(dir, tz="") {
  wd<-getwd()
  setwd(dir)
  
  dir_list<-list.files()
  dataset<-NULL
  
  for (d in dir_list){
  # if the merged dataset doesn't exist, create it
      if (!exists("dataset")){
        dataset <- merge.UGGA.files(d, tz=tz)
      }
      # if the merged dataset does exist, append to it
      if (exists("dataset")){
        temp_dataset <-merge.UGGA.files(d, tz=tz)
        dataset<-rbind(dataset, temp_dataset)
        rm(temp_dataset)
      }
  
  }
  setwd(wd)
  return(dataset)
}

####################################
#Hyytiala data conversion functions#
####################################

hyde_to_POSIX <- function (d, tz="") {
  d$dt<-as.POSIXct(paste(d$Year,"-",d$Month,"-",d$Day," ",d$Hour,":",d$Minute,":",d$Second, sep=""), tz=tz)
  return(d)
}

POSIX_to_hyde <- function (d) {
  d$Year<-as.numeric(substr(d$dt, 1, 4))
  d$Month<-as.numeric(substr(d$dt, 1, 4))
  d$Day<-as.numeric(substr(d$dt, 1, 4))
  d$Hour<-as.numeric(substr(d$dt, 1, 4))
  d$Minute<-as.numeric(substr(d$dt, 1, 4))
  d$Second<-as.numeric(substr(d$dt, 1, 4))
  return(d)
}